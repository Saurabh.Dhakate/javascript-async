function fetchRandomNumbers() {
    return new Promise((res, rej) => {
        console.log('Fetching number...');
        setTimeout(() => {
            let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
            console.log('Received random number:', randomNum);
            res(randomNum);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}

function fetchRandomString() {
    return new Promise((res, rej) => {
        console.log('Fetching string...');
        setTimeout(() => {
            let result = '';
            let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let charactersLength = characters.length;
            for (let i = 0; i < 5; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            console.log('Received random string:', result);
            res(result);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}

// ---------------------------------------- Promises -----------------------------------------------
// Task 1

fetchRandomString().then(console.log)
fetchRandomNumbers().then(console.log)


// Task 2

let sum = 0
fetchRandomNumbers()
    .then((num) => { sum = num + sum; console.log('Sum1 = ', sum) })
    .then(() => fetchRandomNumbers())
    .then((num) => { sum = num + sum; console.log('Sum2 = ', sum) })

// Task 3

Promise.all([fetchRandomNumbers(), fetchRandomString()])
    .then(([num, str]) => console.log(num + str))

// Task 4

let randomNumber = []

function getRandomNumber() {
    fetchRandomNumbers()
        .then((num) => {
            randomNumber.push(num)
            if (randomNumber.length == 10) {
                let sum = randomNumber.reduce((a, b) => (a + b))
                console.log('Sum of 10 Random Numbers = ', sum)
            }
        })
}

let count = 1
while (count <= 10) {
    getRandomNumber()
    count++
}

// ------------------------------------ Async Await ---------------------------------------------

// Task 1

async function asyncTask1() {
    let randomString = await fetchRandomString()
    console.log(randomString)
    let randomNumber = await fetchRandomNumbers()
    console.log(randomNumber)
}

asyncTask1()

// Task 2

async function asyncTask2() {
    let sum = 0
    let randomNumber1 = await fetchRandomNumbers()
    sum = sum + randomNumber1
    console.log('Sum1 = ', sum)
    let randomNumber2 = await fetchRandomNumbers()
    sum = sum + randomNumber2
    console.log('Sum2 = ', sum)
}

asyncTask2()

// Task 3

async function asyncTask3() {
    let array = await Promise.all([fetchRandomNumbers(), fetchRandomString()])
    console.log(array[0] + array[1])
}

asyncTask3()

// Task 4

async function asyncTask4() {
    let promiseArray = []
    while (promiseArray.length < 10) {
        promiseArray.push(fetchRandomNumbers())
    }
    let array = await Promise.all(promiseArray)
    let sum = array.reduce((a, b) => (a + b))
    console.log('Sum of 10 Random Numbers = ', sum)
}

asyncTask4()